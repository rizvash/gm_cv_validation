import os
from imageai.Detection import ObjectDetection

exec_path  = os.getcwd()

detector = ObjectDetection()
detector.setModelTypeAsRetinaNet()
detector.setModelPath(os.path.join(
    exec_path, "resnet50_coco_best_v2.0.1.h5")
)

detector.loadModel()

list_detect_obj = detector.detectObjectsFromImage(
    input_image=os.path.join(exec_path,"objects.jpg"),
    output_image_path=os.path.join(exec_path,"detect_objects.jpg"),
    #minimum_percentage_probability=30,
   # display_percentage_probability=False, #True
   # display_object_name=False
)
