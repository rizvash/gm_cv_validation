import pytest
from src.common.imgdetector import ImgDetector


@pytest.fixture(scope="session")
def img_detector():
    return ImgDetector()


@pytest.mark.usefixtures("env")
def pytest_report_header(config):
    if config.getoption("verbose") > 0:
        return [f"Environment Started! Lets test CV!!! ..."]