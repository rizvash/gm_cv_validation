import os
import allure
import pytest
from src.common import logger
from src.common.repo.images import img_dir
from src.common.log_decorator import automation_logger


@allure.feature("Label")
@pytest.mark.usefixtures("img_detector")
@pytest.mark.one
class TestHowManyCars(object):

    @allure.testcase("test_how_many_cars_exist", name="Carss")
    @automation_logger(logger)
    def test_how_many_cars_exist(self, img_detector):
        label = 'cars_1.jpg'
        gvn_label = os.path.join(img_dir, label)
        obj_ = img_detector.predict_img(gvn_label)
        filtered_obj = [i for i in obj_ if i["label"] == "car"]
        assert len(filtered_obj) >= 8
        logger.logger.info("Test is passed!")
