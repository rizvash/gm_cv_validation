import os
import allure
import pytest
from src.common import logger
from src.common.repo.images import img_dir
from src.common.log_decorator import automation_logger


@allure.feature("Label")
@pytest.mark.usefixtures("img_detector")
@pytest.mark.two
class TestStopSignal(object):

    @allure.testcase("Check that Stop Signal appears.s", name="Stop Signal")
    @automation_logger(logger)
    def test_that_stop_signal_exist(self, img_detector):
        label = 'cars_1.jpg'
        logger.logger.info(f"label is {label}")
        gvn_label = os.path.join(img_dir, label)
        obj_ = img_detector.predict_img(gvn_label)
        entity_, assert_ = img_detector.find_object(obj_, obj_label='stop sign')
        assert entity_ is not None and assert_ is True
        logger.logger.info("Test is passed!")
