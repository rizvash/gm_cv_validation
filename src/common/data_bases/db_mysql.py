#It is a TEMPLATE
import pymysql

#from config_definitions import BaseConfig
from src.common import logger
from src.common.log_decorator import automation_logger


BaseConfig = 'BaseConfig'
class SqlDb:
    # Connector for SQL DB.
    connection = pymysql.connect(host=BaseConfig.SQL_HOST, port=int(BaseConfig.SQL_PORT),
                                 user=BaseConfig.SQL_USERNAME, passwd=BaseConfig.SQL_PASSWORD,
                                 database=BaseConfig.SQL_DB, charset='utf8mb4', autocommit=True)

    @classmethod
    @automation_logger(logger)
    def run_mysql_query(cls, query):
        """
        To run SQL query on MySQL DB.
        :param query: SQL query.
        :return: data from executed query.
        """
        # Ignore "Not closed socket connection" warning.
        # warnings.simplefilter("ignore", ResourceWarning)
        # SQL client- connector for MySQL DB.
        with cls.connection.cursor() as cursor:
            cursor.execute(query)
            rows = cursor.fetchall()
            cursor.close()
            if rows:
                return rows

    @staticmethod
    def run_mysql_query_slave(query):
        connection = pymysql.connect(host=BaseConfig.SQL_SLAVE_HOST, port=int(BaseConfig.SQL_PORT),
                                     user=BaseConfig.SQL_USERNAME, passwd=BaseConfig.SQL_PASSWORD,
                                     database=BaseConfig.SQL_DB, charset='utf8mb4', autocommit=True)
        with connection.cursor() as cursor:
            cursor.execute(query)
            rows = cursor.fetchall()
            cursor.close()
            if rows:
                return rows