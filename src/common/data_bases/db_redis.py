import json
import time
import redis
import datetime
from VIRTUAL_ENV_SCOPES.venv_examples_tests.base import logger
#from config_definitions import BaseConfig
from VIRTUAL_ENV_SCOPES.venv_examples_tests.base.log_decorator import automation_logger


BaseConfig="BaseConfig"

class RedisDb:
    # Connector for Redis DB.
    redis_client = redis.StrictRedis(host=BaseConfig.REDIS_HOST, decode_responses=True, port=BaseConfig.REDIS_PORT)

    @classmethod
    @automation_logger(logger)
    def subscribe_channel_by_pattern(cls, pattern):
        """
        Subscribe to events which are published to channel by pattern
        :param pattern: pattern for subscribe as string
        """
        try:
            p = cls.redis_client.pubsub()
            p.psubscribe(f"{pattern}")
            return p
        except Exception as e:
            logger.logger.error(f"subscribe_channel_by_pattern has failed: {e}")
            raise e


    @classmethod
    @automation_logger(logger)
    def delete_redis_key(cls, key):
        """
        Deletes the provided Key  in Redis.
        Related value is removed as well
        :param key:
        """
        key = str(key) if not isinstance(key, str) else key
        try:
            cls.redis_client.delete(key)
        except Exception as e:
            logger.logger.error(f"Failed to delete the provided Redis key: {e}")
            raise e
