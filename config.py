import configparser
import os

from src.common.repo import repo_dir


def get_parser(config):
    parser = configparser.ConfigParser()
    with open(config, mode='r', buffering=-1, closefd=True):
        parser.read(config)
        return parser

image_dir = repo_dir


class BaseConfig:

    config_file = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config.cfg')
    parser = get_parser(config_file)

    IMAGE_OUTPUT = image_dir + parser.get('PATH', 'output_image')
