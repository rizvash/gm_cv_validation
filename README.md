CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Location
 * Configuration
 * Enable PyTest for the project
 * Technologies, CV tools/ libraries
 * Recommended plugins
 * Requirements
 * TESTS RUN and Allure report
 * Test Groups templates
 * Troubleshooting
 * Git Configuration
 * Python Installation
 * Maintainers

INTRODUCTION
------------

The "gm-qa-automation"- is a first overview for home exam.


LOCATION
---------

- SSH: git@gitlab.com:rizvash/gm_cv_validation.git
- HTTPS: https://gitlab.com/rizvash/gm_cv_validation.git


CONFIGURATION
-------------

The structure of the project separeted on two domaine zone:
1) The home assigment: (venv) gm-qa-automation
  
2) venv_playground_image_ai & venv_playground_luminoth - just a playgrounds
 - venv_luminoth: CLI playground for Computer Vision toolkit https://luminoth.ai/
 - venv_pg_image_ai: playground for ImageAI. Computer Vision library https://imageai.readthedocs.io/en/latest/

** each of the virtual environments contains its own list of dependencies specified 
in the "requirements.txt" file.
 
For activate virtual env:
- run in terminal `actvate` from "source bin/activate" *from bash*
or
- File -> Settings ->  Project <project name> -> Project Interpreter

* To install all dependencies run command (for chosen env):
* $ pip3 install -r requirements.txt


ENABLE PyTest FOR PROJECT
-------------------------

- Open the Settings/Preferences | Tools | Python Integrated Tools settings dialog as described in Choosing Your Testing Framework.
- In the Default test runner field select pytest.
- Click OK to save the settings.

TECHNOLOGIES, COMPUTER VISION TOOLS/LIBRARIES
----------------------------------------------
https://hub.packtpub.com/top-10-computer-vision-tools/

- PyTest - advanced test framework.
- Luminoth - open source toolkit for computer vision.
- TensorFlow - open source library for ML models. 
- Pandas - a library for data manipulation and analysis
- NumPy - a library for adding support for large, multi-dimensional arrays and matrices, along with a large collection of high-level mathematical functions to operate on these arrays
- Matplotlib
- Opencv

- Google Cloud and Mobile Vision 
- Amazon Rekognition
- Microsoft Azure Computer Vision

- redis - access to Redis DB.
- PyMySQL - access to SQL DB.
- allure-pytest - reporting.
- beautifulsoup4 - work with HTML.


RECOMMENDED PLUGINS
-------------------
- bashsupport - ability to execute shell/bash scripts.
- multirun - ability to run several configurations.
- .gitignore - prevents redundant uploads.
- GitLab - projects integration with remote repo and VCS.
- CSV - plugin to support csv files.
- Docker - for docker integration.
- CMD support.


REQUIREMENTS
------------

1. PyCharm IDEA installed.
2. Python 3.5 - 3.7 or later installed.
3. Allure installed in folder (/home/user_name/Allure) https://github.com/allure-framework/allure2/releases/tag/2.13.0
4. Python virtualenvironment installed out of the project and activated.
5. Python interpreter configured.
6. Project requirements installed.
7. Project plugins installed.


TESTS RUN and Allure report
---------
** Run from project root (gm-qa-automation)

1 Run tests suite:
* $ pytest tests/test_suite_1 --alluredir=src/repository/allure_results

2 Run tests as a package /(by group 'one', 'two') :

* $ pytest . -m one --alluredir=src/repository/allure_results
* $ pytest . -m two --alluredir=src/repository/allure_results

3 Run specific test:
* $ pytest -v tests/test_suite_1/check_how_many_cars_exists_test.py --alluredir=src/repository/allure_results

4 Generate temporary allure report:
* $ src/repository/scripts/allure_results.sh
  
5 Generate report and open the result WEB page:
* $ src/repository/scripts/allure_report.sh
  


Test Groups templates:
-------------------

1. smoke - 
2. accuracy - 
3. sensor data validation - 
4. regression
5. tests related to DBs
6. services
7. perfomance 
8. integration
9. e2e - end to end tests
10. api
11. negative 


TROUBLESHOOTING
---------------

Docker / Solving Network conflict:
-----------------------------------
* If your docker build fails such as:
"ERROR: cannot create network",
(br-c868a505e7d9): "conflicts with network",
(br-992ca1654879): "networks have overlapping IPv4"

* Look on docker network processes:
$ docker network ls

* Find from returned list conflicted ID:
NETWORK ID          NAME                        DRIVER              SCOPE
10060c77e51a        bridge                      bridge              local

* Remove by ID:
$ docker network rm

Kafka:
------
1 Installation guide:

https://kafka.apache.org/quickstart

https://medium.com/@shaaslam/installing-apache-kafka-on-windows-495f6f2fd3c8


Git Configuration:
------------------
* $ git init

* $ git status

* $ git config --global --list

* $ git config --global user.name ""

* $ git config --global user.email ""

* $ cat ~/.gitconfig

* $ git config --global help.autocorrect 1

* $ git config core.autorlf true/false


Python Installation:  
--------------------
https://www.python.org/downloads/windows/


* install pip:
$ python get-pip.py

* install virtual environment:
$ pip install virtualenv

* create virtual environment:
$ virtualenv venv --python=python3.7

* activate environment for Windows:
$ venv\Scripts\activate

* activate environment for Unix:
$ source venv/bin/activate

* list all packages installed in the environment:
$ pip freeze

* upgrade pip:  
$ python -m pip install --upgrade pip


MAINTAINERS
-----------

* Rizvash Iliya <rizvash.i@gmail.com>
